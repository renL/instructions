
# Simulation Microcontroller

## "Wokwi" Online Arduino Simulator
 zum online Simulator: <https://www.wokwi.com>

Mit Wokwi kannst du deine Microcontroller-Projekte simulieren um bspw. einzelne Codeabschnitte zu testen. 
Dazu kannst du zwischen gängigen Arduino-Boards, ESP32-Board und weiteren Boards wählen.


## "TinkerCAD" Online Simulator
 zum online Simulator: <https://www.tinkercad.com>

 Mit TinkerCAD kannst du verschiedene Projekte erstellen. Neben 3D-Entwürfen kannst du auch Schaltkreise erstellen.
 Dsimulieren um bspw. einzelne Codeabschnitte zu testen. 
